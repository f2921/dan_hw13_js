document.addEventListener("DOMContentLoaded", function() {
  
  let stopBtn = document.querySelector('.stop_btn');
  let restoreBtn = document.querySelector('.restore_btn');	
  let imagesData = ["./img/2.jpg","./img/3.jpg","./img/4.png","./img/1.jpg"];

  let index = 0;
  const imgElement = document.querySelector(".image-to-show");

  function changeImages() {
    imgElement.src = imagesData[index];
    index == 3 ? index = 0 : index++;
  }

  window.onload = function() {
    let mainInterval = setInterval(changeImages, 3000);
    stopBtn.addEventListener('click',() => {
      
       clearInterval(mainInterval);
     
    })
    
    restoreBtn.addEventListener('click',() => {
      
      mainInterval =  setInterval(changeImages, 3000);
     
    })
   
  };

})
